<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class ModuleVilius extends Module
{
//    protected $text = '';
    public function __construct()
    {
        $this->name = 'modulevilius';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Vilius Bruzda';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => '1.7.99',
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Module Vilius');
        $this->description = $this->l('Viliaus Module');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('VILIUS_MODULE')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        return (
            parent::install()
            && $this->registerHook('displayleftColumn')
            && $this->registerHook('header')
            && Configuration::updateValue('VILIUS_MODULE', 'my friend')
            && Configuration::updateValue('VILIUS_MODULE_CHECKBOX', 1)
        );
    }

    public function uninstall()
    {
        return (
            parent::uninstall()
            && Configuration::deleteByName('VILIUS_MODULE')
            && Configuration::deleteByName('VILIUS_MODULE_CHECKBOX')
        );
    }

    public function hookDisplayLeftColumn($params)
    {
        return Configuration::get('VILIUS_MODULE');
    }

    public function getContent()
    {
        $this->_html = ' <h2>'.$this->displayName.'</h2>';
        $this->_postProcess();
        $this->_html .= $this->renderShopCommentsForm();
        return $this->_html . $this->initList();
    }

    protected function _postProcess()
    {
        if(Tools::isSubmit('submitViliusForm'))
        {
            $settings_values = $this->getSettingsFieldsValues();
            foreach($settings_values as $key => $value)
            {
                if(Tools::getValue($key) !== false)
                {
                    Configuration::updateValue($key, Tools::getValue($key));
                }
            }

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'');
        }
    }

    protected function renderShopCommentsForm()
    {
        $settings_fields_form = $this->FormFields();
        $helper = new HelperForm();
        $helper->fields_value = $this->getSettingsFieldsValues();
        $settings_form = $helper->generateForm(array($settings_fields_form));
        return $settings_form;
    }

    protected function getSettingsFieldsValues()
    {
        $fields = array();
        $fields['VILIUS_MODULE'] = Configuration::get('VILIUS_MODULE');
        $fields['VILIUS_MODULE_CHECKBOX'] = Configuration::get('VILIUS_MODULE_CHECKBOX');
        return $fields;
    }

    protected function FormFields()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Custom Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => 'Vilius Module field: ',
                        'name' => 'VILIUS_MODULE',
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'VILIUS_MODULE_CHECKBOX',
                        'label' => 'Vilius SWITCH',
                        'values' => array(
                            array(
                                'id' => 'VILIUS_MODULE_CHECKBOX_on',
                                'value' => 1,
                                'label' => $this->trans('Enabled', array(), 'Admin.Global'),
                            ),
                            array(
                                'id' => 'VILIUS_MODULE_CHECKBOX_off',
                                'value' => 0,
                                'label' => $this->trans('Disabled', array(), 'Admin.Global'),
                            ),
                        ),
                    )

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name'=>'submitViliusForm'
                )
            )
        );
    }

    private function initList()
    {
        $this->fields_list = array(
            'id_category' => array(
                'title' => $this->l('Id'),
                'width' => 140,
                'type' => 'text',
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'width' => 140,
                'type' => 'text',
            ),
        );
        $helper = new HelperList();

        $helper->shopLinkType = '';
        $helper->simple_header = true;
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('edit', 'delete', 'view');
//        NO DB, JUST ARRAY
//        $database_data = [
//            ['id' => 1, 'name' => 'Vilius'],
//            ['id' => 2, 'name' => 'Vilius'],
//            ['id' => 3, 'name' => 'Vilius'],
//            ['id' => 4, 'name' => 'Vilius']
//        ];
//          DB NOT GETTING DATA
        $database_data = [];
        $sql = 'SELECT * FROM '. _DB_PREFIX_ .'product_supplier';
        $results = Db::getInstance()->ExecuteS($sql);
        if (empty($results)) {
            $database_data= 'Nothing';
        } else {
            foreach ($results as $result){
                    array_push($database_data, ['id_category' => $result['id_product_supplier'], 'name' => $result['product_supplier_reference']]);
            }
        }

        $helper->identifier = 'id_category';
        $helper->show_toolbar = true;
        $helper->title = 'HelperList';
        $helper->table = $this->name.'_categories';

        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        return $helper->generateList($database_data, $this->fields_list);
    }
}