<?php
/* Smarty version 3.1.43, created on 2022-04-05 18:27:35
  from '/home/magnusfourteen/public_html/prestashop17.local/themes/classic/templates/catalog/_partials/product-flags.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_624c5fe7188c70_48311276',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2f891df92903c24e4ca03212fb8b92d97cc1f724' => 
    array (
      0 => '/home/magnusfourteen/public_html/prestashop17.local/themes/classic/templates/catalog/_partials/product-flags.tpl',
      1 => 1649172212,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_624c5fe7188c70_48311276 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->compiled->nocache_hash = '12792341624c5fe7181b09_32217617';
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_742372679624c5fe7182cb7_41949814', 'product_flags');
?>

<?php }
/* {block 'product_flags'} */
class Block_742372679624c5fe7182cb7_41949814 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_flags' => 
  array (
    0 => 'Block_742372679624c5fe7182cb7_41949814',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <ul class="product-flags js-product-flags">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product']->value['flags'], 'flag');
$_smarty_tpl->tpl_vars['flag']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['flag']->value) {
$_smarty_tpl->tpl_vars['flag']->do_else = false;
?>
            <li class="product-flag <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['type'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['label'], ENT_QUOTES, 'UTF-8');?>
</li>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </ul>
<?php
}
}
/* {/block 'product_flags'} */
}
