<?php
/* Smarty version 3.1.43, created on 2022-04-05 18:27:35
  from '/home/magnusfourteen/public_html/prestashop17.local/themes/classic/templates/_partials/helpers.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_624c5fe7321405_50553355',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4272cab8bdfe2a5103f24cd9d9fe66abd0dd59d6' => 
    array (
      0 => '/home/magnusfourteen/public_html/prestashop17.local/themes/classic/templates/_partials/helpers.tpl',
      1 => 1649172212,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_624c5fe7321405_50553355 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/home/magnusfourteen/public_html/prestashop17.local/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/42/72/ca/4272cab8bdfe2a5103f24cd9d9fe66abd0dd59d6_2.file.helpers.tpl.php',
    'uid' => '4272cab8bdfe2a5103f24cd9d9fe66abd0dd59d6',
    'call_name' => 'smarty_template_function_renderLogo_1914661988624c5fe7317317_03021384',
  ),
));
?> 

<?php }
/* smarty_template_function_renderLogo_1914661988624c5fe7317317_03021384 */
if (!function_exists('smarty_template_function_renderLogo_1914661988624c5fe7317317_03021384')) {
function smarty_template_function_renderLogo_1914661988624c5fe7317317_03021384(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
">
    <img
      class="logo img-fluid"
      src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
"
      alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"
      width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
"
      height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
">
  </a>
<?php
}}
/*/ smarty_template_function_renderLogo_1914661988624c5fe7317317_03021384 */
}
