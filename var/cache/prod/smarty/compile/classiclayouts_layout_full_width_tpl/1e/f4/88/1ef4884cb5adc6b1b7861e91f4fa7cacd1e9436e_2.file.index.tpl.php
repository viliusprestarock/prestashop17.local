<?php
/* Smarty version 3.1.43, created on 2022-04-05 18:27:35
  from '/home/magnusfourteen/public_html/prestashop17.local/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_624c5fe729b682_48323216',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1ef4884cb5adc6b1b7861e91f4fa7cacd1e9436e' => 
    array (
      0 => '/home/magnusfourteen/public_html/prestashop17.local/themes/classic/templates/index.tpl',
      1 => 1649172212,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_624c5fe729b682_48323216 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1297585622624c5fe728fa36_57327541', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_143952744624c5fe7291435_56208442 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_696313554624c5fe72955d6_58659320 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_2104610919624c5fe7293c84_63708017 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_696313554624c5fe72955d6_58659320', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_1297585622624c5fe728fa36_57327541 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_1297585622624c5fe728fa36_57327541',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_143952744624c5fe7291435_56208442',
  ),
  'page_content' => 
  array (
    0 => 'Block_2104610919624c5fe7293c84_63708017',
  ),
  'hook_home' => 
  array (
    0 => 'Block_696313554624c5fe72955d6_58659320',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_143952744624c5fe7291435_56208442', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2104610919624c5fe7293c84_63708017', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
