<?php
/* Smarty version 3.1.43, created on 2022-04-05 18:27:35
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_624c5fe754c0a5_55484175',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1649172212,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_624c5fe754c0a5_55484175 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/home/magnusfourteen/public_html/prestashop17.local/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/42/72/ca/4272cab8bdfe2a5103f24cd9d9fe66abd0dd59d6_2.file.helpers.tpl.php',
    'uid' => '4272cab8bdfe2a5103f24cd9d9fe66abd0dd59d6',
    'call_name' => 'smarty_template_function_renderLogo_1914661988624c5fe7317317_03021384',
  ),
));
?><div class="col-md-6 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Products</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_1" data-toggle="collapse">
        <span class="h3">Products</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_1" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?controller=prices-drop"
                title="Our special products"
                            >
              Prices drop
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?controller=new-products"
                title="Our new products"
                            >
              New products
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?controller=best-sales"
                title="Our best sales"
                            >
              Best sales
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Our company</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_2" data-toggle="collapse">
        <span class="h3">Our company</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_2" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?id_cms=1&amp;controller=cms&amp;id_lang=1"
                title="Our terms and conditions of delivery"
                            >
              Delivery
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?id_cms=2&amp;controller=cms&amp;id_lang=1"
                title="Legal notice"
                            >
              Legal Notice
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?id_cms=3&amp;controller=cms&amp;id_lang=1"
                title="Our terms and conditions of use"
                            >
              Terms and conditions of use
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?id_cms=4&amp;controller=cms&amp;id_lang=1"
                title="Learn more about us"
                            >
              About us
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?id_cms=5&amp;controller=cms&amp;id_lang=1"
                title="Our secure payment method"
                            >
              Secure payment
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?controller=contact"
                title="Use our form to contact us"
                            >
              Contact us
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?controller=sitemap"
                title="Lost ? Find what your are looking for"
                            >
              Sitemap
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://localhost/prestashop17.local/index.php?controller=stores"
                title=""
                            >
              Stores
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }
}
